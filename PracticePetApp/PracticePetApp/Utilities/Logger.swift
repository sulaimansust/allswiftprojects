//
//  Logger.swift
//  PracticePetApp
//
//  Created by Sulaiman Khan on 11/21/17.
//  Copyright © 2017 IPVision. All rights reserved.
//

import Foundation

class Logger {
    class func log(_ tag:String, _ message: String){
        print("\(tag) : \(message)")
    }
}
