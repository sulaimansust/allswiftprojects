//
//  BoughtTableViewController.swift
//  PracticePetApp
//
//  Created by Sulaiman Khan on 11/20/17.
//  Copyright © 2017 IPVision. All rights reserved.
//

import UIKit


class BoughtTableViewController: UIViewController {
    let TAG:String = "BoughtTableViewController"
    
    
    @IBOutlet weak var boughtTableView: UITableView!
    
    override func viewDidLoad() {
        Logger.log(TAG, "viewDidLoad \(PetDataProviderModel.sharedProvider.boughtPets.count)")
        
        boughtTableView.register(UINib.init(nibName: "PetTableViewCell", bundle: nil), forCellReuseIdentifier: "PetTableViewCell")
        boughtTableView.rowHeight=110
        
        boughtTableView.reloadData()
    }
    
    func pushToPetViewController() {
        Logger.log(TAG, "pushToPetViewController")
        let index = boughtTableView.indexPathForSelectedRow
        let indexNumber = index?.row
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "petViewController") as! PetViewController
        viewController.pet = PetDataProviderModel.sharedProvider.boughtPets[indexNumber!]
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}




//MARK: - UITableViewDelegate

extension BoughtTableViewController:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        pushToPetViewController()
        let cell:UITableViewCell = (self.boughtTableView?.cellForRow(at: indexPath))!
        cell.setSelected(false, animated: true)
    }
}


//MARK: - UITableViewDataSource

extension BoughtTableViewController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        Logger.log(TAG, "numberOfRowsInSection \(PetDataProviderModel.sharedProvider.boughtPets.count)")
        return PetDataProviderModel.sharedProvider.boughtPets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        Logger.log(TAG, "cellForRowAt")

        let petData = PetDataProviderModel.sharedProvider.boughtPets[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PetTableViewCell", for: indexPath) as! PetTableViewCell
        
        cell.petImageView?.image = UIImage(named: petData.imageName)
        cell.petImageView?.layer.masksToBounds = true
        cell.petImageView?.layer.cornerRadius = 5
        cell.titleLabel?.text = petData.name
        cell.subtitleLabel?.text = petData.type
        
        return cell
    }
    
    
}
