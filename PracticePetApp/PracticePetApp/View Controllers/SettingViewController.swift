//
//  SettingViewController.swift
//  PracticePetApp
//
//  Created by Sulaiman Khan on 11/25/17.
//  Copyright © 2017 IPVision. All rights reserved.
//

import UIKit

class SettingsViewController: UITableViewController {
    @IBOutlet weak var themeSelector: UISegmentedControl!
    
    
    override func viewDidLoad() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(dismissAnimated))
    }
    

}


private extension SettingsViewController{
    @objc func dismissAnimated(){
        dismiss(animated: true)
    }
    
    @IBAction func applyTheme(_ sender: UIButton) {
        
    }
}
