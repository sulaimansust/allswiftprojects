//
//  PetTableViewCell.swift
//  PracticePetApp
//
//  Created by Sulaiman Khan on 11/20/17.
//  Copyright © 2017 IPVision. All rights reserved.
//

import Foundation
import UIKit
class PetTableViewCell: UITableViewCell {
    @IBOutlet weak var petImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
}

