//
//  PetViewController.swift
//  PracticePetApp
//
//  Created by Sulaiman Khan on 11/25/17.
//  Copyright © 2017 IPVision. All rights reserved.
//

import UIKit

protocol PetViewControllerDelegate: class {
    func petViewController(_ controller: PetViewController, didAdoptPet pet: Pet)
}

class PetViewController:UIViewController {
    
    @IBOutlet var petImageView : UIImageView!
    weak var delegate: PetViewControllerDelegate?
    var pet:Pet!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = pet.name
        petImageView.image = UIImage(named: pet.imageName)
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Adopt", style: .plain, target: self, action: #selector(adopt))
    }
    
    func pushToBuyConfirmationViewController(){
        
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "buyConfirmation") as! BuyConfirmationViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}


// MARK: - Actions adopt

private extension PetViewController{
    @objc func adopt() {
        pushToBuyConfirmationViewController()
        delegate?.petViewController(self, didAdoptPet: pet)
    }
}
