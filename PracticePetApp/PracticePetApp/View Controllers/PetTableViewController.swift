//
//  PetTableViewController.swift
//  PracticePetApp
//
//  Created by Sulaiman Khan on 11/20/17.
//  Copyright © 2017 IPVision. All rights reserved.
//

import Foundation
import UIKit

class PetTableViewController : UIViewController {
    let TAG: String = "PetTableViewController"
    @IBOutlet weak var petListTableView: UITableView!
    var tableViewData = PetDataProviderModel.sharedProvider.pets
    
    override func viewDidLoad() {
        Logger.log(TAG, "viewDidLoad")
        self.petListTableView.register(UINib.init(nibName: "PetTableViewCell", bundle: nil), forCellReuseIdentifier: "PetTableViewCell")
        petListTableView.rowHeight=110
        settingUPNavigationBar()
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        Logger.log(TAG, "viewWillDisappear \(PetDataProviderModel.sharedProvider.myPetsCount())")
    }
    
    
    func settingUPNavigationBar() {
        Logger.log(TAG, "settingUPNavigationBar")
        
        
        let titleImageView = UIImageView(image: UIImage(named: "catdog"))
        titleImageView.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        titleImageView.contentMode = .scaleAspectFit
        navigationItem.titleView = titleImageView
        
        let searchButton = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(search))
        navigationItem.rightBarButtonItem = searchButton
        
        let settingsButton = UIBarButtonItem(image: UIImage(named: "settings"), style: .plain, target: self, action: #selector(setting))
        navigationItem.leftBarButtonItem = settingsButton
        
    }
    
    func pushToPetViewController() {
        Logger.log(TAG, "pushToPetViewController")
        let index = petListTableView.indexPathForSelectedRow
        let indexNumber = index?.row
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "petViewController") as! PetViewController
        viewController.pet = tableViewData[indexNumber!]
        viewController.delegate=self
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    

}
//MARK: - UITableViewDataSource

extension PetTableViewController : UITableViewDataSource{
    @objc internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        Logger.log(TAG, "numberOfRowInSection")
        return tableViewData.count
    }
    
    @objc internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        Logger.log(TAG, "cellForRowAtIndexPath")
        let petData = tableViewData[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PetTableViewCell", for: indexPath) as! PetTableViewCell
        
        cell.petImageView?.image = UIImage(named: petData.imageName)
        cell.petImageView?.layer.masksToBounds = true
        cell.petImageView?.layer.cornerRadius = 5
        cell.titleLabel?.text = petData.name
        cell.subtitleLabel?.text = petData.type
        
        return cell
        
    }
}

//MARK: - UITableViewDelegate

extension PetTableViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        pushToPetViewController()
        let cell:UITableViewCell = (self.petListTableView?.cellForRow(at: indexPath))!
        cell.setSelected(false, animated: true)
    }
    
}


//MARK: - Actions

private extension PetTableViewController{
    
    
    
    @objc func search() {
        
    }
    
    
    @objc func setting(){
        Logger.log(TAG, "setting button is clicked")
        guard  let viewController = self.storyboard?.instantiateViewController(withIdentifier: "settingViewController") as! SettingsViewController? else{
            return
        }
        viewController.modalPresentationStyle = .popover
        viewController.modalTransitionStyle = .coverVertical
        viewController.popoverPresentationController?.delegate = self
        present(viewController, animated: true)
    }
    
}

//MARK: - PetViewControllerDelegate

extension PetTableViewController: PetViewControllerDelegate{
    
    func petViewController(_ controller: PetViewController, didAdoptPet pet: Pet) {
        PetDataProviderModel.sharedProvider.adopt(pet: pet)
    }
    
}
//MARK: - UIPopoverPresentationControllerDelegate

extension PetTableViewController: UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.overCurrentContext
    }
    func presentationController(_ controller: UIPresentationController, viewControllerForAdaptivePresentationStyle style: UIModalPresentationStyle) -> UIViewController? {
        return UINavigationController(rootViewController: controller.presentedViewController)
    }
}
