//
//  PetDataProvider.swift
//  PracticePetApp
//
//  Created by Sulaiman Khan on 11/20/17.
//  Copyright © 2017 IPVision. All rights reserved.
//

import Foundation

class PetDataProviderModel {
    static let sharedProvider = PetDataProviderModel()
    
    let TAG:String = "PetDataProviderModel"

    let pets:[Pet]
    
    var boughtPets:[Pet]
    
    init() {
        self.pets = [Pet(name: "Rusty", type: "Golden Retriever", imageName: "pet0"),
                     Pet(name: "Max", type: "Mixed Terrier", imageName: "pet1"),
                     Pet(name: "Lucifer", type: "Freaked Out", imageName: "pet2"),
                     Pet(name: "Tiger", type: "Sensitive Whiskers", imageName: "pet3"),
                     Pet(name: "Widget", type: "Mouse Catcher", imageName: "pet4"),
                     Pet(name: "Wiggles", type: "Border Collie", imageName: "pet5"),
                     Pet(name: "Clover", type: "Mixed Breed", imageName: "pet6"),
                     Pet(name: "Snow White", type: "Black Cat", imageName: "pet7")]
        self.boughtPets = []
    }

    func adopt(pet : Pet) {
        Logger.log(TAG, "adopt "+pet.name)
        self.boughtPets.append(pet)
        print(myPetsCount())
    }
    
    func totalPetsCount() ->  Int {
        return pets.count
    }
    
    func myPetsCount() -> Int {
        return boughtPets.count
    }
    


}
