//
//  Pet.swift
//  PracticePetApp
//
//  Created by Sulaiman Khan on 11/20/17.
//  Copyright © 2017 IPVision. All rights reserved.
//

import Foundation

struct Pet {
    let name:String
    let type:String
    let imageName:String
}

//Now we are extending the default equal equal method for the Pet structure type

extension Pet: Equatable{
    static func ==(lhs: Pet, rhs: Pet) -> Bool {
        return lhs.name == rhs.name && lhs.type==rhs.type
    }
}
