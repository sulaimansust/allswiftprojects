//
//  SearchVc+URLSessionDelegates.swift
//  HalfTunes
//
//  Created by Sulaiman Khan on 3/27/18.
//  Copyright © 2018 Ray Wenderlich. All rights reserved.
//

import Foundation
import UIKit


extension SearchViewController : URLSessionDownloadDelegate {
	
	
	func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
		print("Finished downloading to \(location)")
		
		guard let sourceUrl = downloadTask.originalRequest?.url else {
			return
		}
		
		let download = downloadService.activeDownloads[sourceUrl]
		downloadService.activeDownloads[sourceUrl] = nil
		
		let destinationURL = localFilePath(for: sourceUrl)
		
		print("destinationUrl == \(destinationURL) ")
		
		
		let fileManager = FileManager.default
		try? fileManager.removeItem(at: destinationURL)
		do {
			try fileManager.copyItem(at: location, to: destinationURL)
			download?.track.downloaded = true
		} catch let error {
			print("Could not copy file to disk: \(error.localizedDescription)")
		}
		
		if let index = download?.track.index {
			DispatchQueue.main.async {
				self.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
			}
		}
		
	}
	
	
	
	
}
