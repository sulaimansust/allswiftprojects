//
//  ViewController.swift
//  AudioUnitRecAndPlay
//
//  Created by Sulaiman Khan on 1/24/18.
//  Copyright © 2018 IPVision. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	let TAG:String = "ViewController"
	var audioPlayer: AudioUnitPlayer!
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		audioPlayer = AudioUnitPlayer()
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

	@IBAction func resumePlayer(_ sender: UIButton) {
		Log.log(TAG, "resumePlayer")
		audioPlayer.play()
	}
	@IBAction func record(_ sender: UIButton) {
		Log.log(TAG, "record")
	}
	
	@IBAction func play(_ sender: UIButton) {
		Log.log(TAG, "play")
		audioPlayer.play()
	}
	@IBAction func pausePlayer(_ sender: UIButton) {
		Log.log(TAG, "pausePlayer")
		audioPlayer.stop()
	}
}

