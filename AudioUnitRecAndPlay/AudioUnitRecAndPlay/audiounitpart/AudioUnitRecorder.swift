//
//  AudioUnitRecorder.swift
//  AudioUnitRecAndPlay
//
//  Created by Sulaiman Khan on 1/24/18.
//  Copyright © 2018 IPVision. All rights reserved.
//

import Foundation
import AudioUnit
import AudioToolbox
import AVFoundation

class AudioUnitPlayer {
		let TAG = "AudioUnitPlayer"
	var _samples:[Float] = [Float] (repeating: 0.0 , count: 64)
	var _audioUnit: AudioUnit? = nil
	var _x: Float = 0
	let _sampleRate:Double = 44100
	
	
	let playCallback: AURenderCallback = {
		(inRefCon: UnsafeMutableRawPointer,
		ioActionFlags: UnsafeMutablePointer<AudioUnitRenderActionFlags>,
		inTimeStamp: UnsafePointer<AudioTimeStamp>,
		inBusNumber: UInt32,
		inNumberFrames: UInt32,
		ioData: UnsafeMutablePointer<AudioBufferList>?)
		
		in
		let audioUnitPlayer:AudioUnitPlayer = Unmanaged<AudioUnitPlayer>.fromOpaque(inRefCon).takeUnretainedValue()
		audioUnitPlayer.render(inNumberFrames, ioData: ioData)
		return noErr
	}
	
	let recordCallback: AURenderCallback = {
		(	inRefCon: UnsafeMutableRawPointer,
			ioActionFlags: UnsafeMutablePointer,
			inTimeStamp: UnsafePointer<AudioTimeStamp>,
			inBusNumber: UInt32,
			inNumberFrames: UInt32,
			ioData: UnsafeMutablePointer<AudioBufferList>?)
		in
		let audioRecorder = Unmanaged<AudioUnitPlayer>.fromOpaque(inRefCon).takeUnretainedValue()
		
		var  audioBuffer:AudioBuffer = AudioBuffer()
		audioBuffer.mNumberChannels = 1
		audioBuffer.mDataByteSize = inNumberFrames * 2
		audioBuffer.mData = malloc(Int(inNumberFrames * 2))
		
		var audioBufferList: AudioBufferList = AudioBufferList()
		audioBufferList.mNumberBuffers = 1
//		audioBufferList
		return 1
	}
	
	
	init() {
		let subType = kAudioUnitSubType_RemoteIO
		
		var audioComponentDescription = AudioComponentDescription(componentType: kAudioUnitType_Output,
		                                                          componentSubType: subType,
		                                                          componentManufacturer: kAudioUnitManufacturer_Apple,
		                                                          componentFlags: 0,
		                                                          componentFlagsMask: 0)
		
		let audioInputComponent = AudioComponentFindNext(nil, &audioComponentDescription)
		
		AudioComponentInstanceNew(audioInputComponent!, &_audioUnit)
		
		AudioUnitInitialize(_audioUnit!)
		
		let audioFormat:AVAudioFormat = AVAudioFormat(standardFormatWithSampleRate: _sampleRate, channels: 2)!
		
		var audioStreamBasicDescription: AudioStreamBasicDescription = audioFormat.streamDescription.pointee
		
		AudioUnitSetProperty(_audioUnit!, kAudioUnitProperty_StreamFormat,
		                     kAudioUnitScope_Input,0,
		                     &audioStreamBasicDescription,
		                     UInt32(MemoryLayout.size(ofValue: audioStreamBasicDescription)))
		
		
	}
	
	

	
	
	fileprivate func render(_ inNumberFrames: UInt32, ioData: UnsafeMutablePointer<AudioBufferList>?){
		let delta:Float = Float(440*2*Double.pi / _sampleRate)
		guard let audioBufferList = UnsafeMutableAudioBufferListPointer(ioData) else {
			return
		}
		
		var x:Float = 0
		
		for buffer:AudioBuffer in audioBufferList {
			x = _x
			
			let capacity = Int(buffer.mDataByteSize)/MemoryLayout<Float>.size
			
			assert(capacity == Int(inNumberFrames))
			
			if let buf: UnsafeMutablePointer<Float> = buffer.mData?.bindMemory(to: Float.self, capacity: capacity){
				for i:Int in 0 ..< Int(inNumberFrames){
					buf[i] = sin(x)
					x+=delta
					if i < 64 {
						_samples[i] = buf[i]
					}
				}
			}
			if audioBufferList.count > 0 {
				_x = x
			}
			
		}
		
	}
	
	func play()  {
		Log.log(TAG,"play")
		let ref: UnsafeMutableRawPointer = UnsafeMutableRawPointer(Unmanaged.passUnretained(self).toOpaque())
		var callbackStruct: AURenderCallbackStruct = AURenderCallbackStruct(inputProc: playCallback, inputProcRefCon: ref)
		
		AudioUnitSetProperty(_audioUnit!, kAudioUnitProperty_SetRenderCallback, kAudioUnitScope_Input, 0, &callbackStruct, UInt32(MemoryLayout.size(ofValue: callbackStruct)))
		AudioOutputUnitStart(_audioUnit!)
	}
	
	func stop() {
		Log.log(TAG,"stop")

		AudioOutputUnitStop(_audioUnit!)
	}
	
	
}
