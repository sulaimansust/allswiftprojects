//
//  Log.swift
//  AudioUnitRecAndPlay
//
//  Created by Sulaiman Khan on 1/27/18.
//  Copyright © 2018 IPVision. All rights reserved.
//

import Foundation

class Log {
	static func log(_ tag: String, _ message:String){
		print("\(tag) --> \(message)")
	}
}
