//
//  DetailsViewController.swift
//  PracticeStormViewer
//
//  Created by Sulaiman Khan on 11/21/17.
//  Copyright © 2017 IPVision. All rights reserved.
//
import Foundation
import UIKit

class DetailsImageViewController: UIViewController {
    
    @IBOutlet weak var detailsImageView: UIImageView!
    var selectedImage: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = selectedImage
        navigationItem.largeTitleDisplayMode  = .never
        
        if let imageName = selectedImage {
            detailsImageView.image = UIImage(named: imageName)
        }
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.hidesBarsOnTap=true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.hidesBarsOnTap=false
    }
    
}
