//
//  ViewController.swift
//  CoreDataPractice
//
//  Created by Sulaiman Khan on 1/28/18.
//  Copyright © 2018 IPVision. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

	var context:NSManagedObjectContext!
	
	var appDelegate:AppDelegate!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		appDelegate = UIApplication.shared.delegate as! AppDelegate
		if #available(iOS 10.0, *) {
			context  = appDelegate.persistentContainer.viewContext
		} else {
			// Fallback on earlier versions
		}
		
		saveOnCoreData()
		
	}
	
	func saveOnCoreData()  {
		//CoreData practice part save a value

		let entity = NSEntityDescription.entity(forEntityName: "Users", in: context)
		
		// creating a user type data
		let newUser = NSManagedObject(entity: entity!, insertInto: context)
		
		newUser.setValue("Sulaiman Khan", forKey: "username")
		newUser.setValue("25", forKey: "age")
		newUser.setValue("123456", forKey: "password")
		
		//saving data here
		do{
			try context.save()
		} catch {
			print("failed saving changes")
		}
	}
	
	func fetchingFromCoreData() {
		//Getting data from core data stack
		let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
		request.returnsObjectsAsFaults = false
		
		do{
			let result = try context.fetch(request)
			
			for data in result as! [NSManagedObject] {
				print(data.value(forKey: "username") as! String)
			}
			
		} catch {
			print("fetching data failed on fetchingFomCoreData")
		}
		
	}
	
	@IBAction func fetchData(_ sender: Any) {
		fetchingFromCoreData()
	}
	@IBAction func saveOnCoreData(_ sender: Any) {
		saveOnCoreData()
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}


}

