//
//  ViewController.swift
//  Transformation
//
//  Created by Shamim Hossain on 3/12/17.
//  Copyright © 2017 IPvisoin Canada Inc. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	@IBOutlet weak var imageView: UIImageView!

	fileprivate func animate() {
		let target = CGPoint.init(x: 240 - self.imageView.frame.width*0.5, y: 80)
		let imageFrame = self.imageView.frame;

		let scaleTransform : CGAffineTransform = CGAffineTransform.init(scaleX: 0.5, y: 0.5)
		let translationTrans : CGAffineTransform = CGAffineTransform.init(translationX: target.x - imageFrame.origin.x, y: target.y - imageFrame.origin.y);
		let scaleAndTranslationTransform = scaleTransform.concatenating(translationTrans);
		let SRT = scaleAndTranslationTransform.rotated(by:CGFloat(Double.pi))
		UIView.animate(withDuration: 3, delay: 0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
			self.imageView.transform = scaleAndTranslationTransform;
		}, completion: nil)
	}

	override func viewDidLoad() {
		super.viewDidLoad()

		animate()
	}

	@IBAction func animate(_ sender: Any) {
		self.imageView.transform = CGAffineTransform.identity
		self.imageView.center = self.view.center;
		animate()
	}
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}


}

