//
//  SearchVC+URLSessionDelegates.swift
//  HalfTunes
//
//  Created by Sulaiman Khan on 12/12/17.
//  Copyright © 2017 Ray Wenderlich. All rights reserved.
//

import Foundation
import UIKit

extension SearchViewController: URLSessionDownloadDelegate{
	func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
		print("Finished downloading to \(location)")
		// 1
		guard let sourceURL = downloadTask.originalRequest?.url else {
			return
		}
		let downlaod = downloadService.activeDownloads[sourceURL]
		downloadService.activeDownloads[sourceURL] = nil
		
		
		// 2
		let destinationURL = localFilePath(for: sourceURL)
		print(destinationURL)
		
		// 3
		
		let fileManager = FileManager.default
		try? fileManager.removeItem(at: destinationURL)
		do{
			try fileManager.copyItem(at: location, to: destinationURL)
			downlaod?.track.downloaded = true
		} catch let error{
			print("Could not copy file to disk: \(error.localizedDescription)")
		}
		
		//4
		if let index = downlaod?.track.index {
			DispatchQueue.main.async {
				self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
			}
		}
		
	}
	
	
}
